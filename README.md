# Circularize images

Create a circular version of an image. Specify the desired width of the output image, and the path to input image(s).

This script is shabbily written at the moment - avoid additional '.'s in the input filepath(s). I will try to fix this soon, but PRs are (of course) welcome!
