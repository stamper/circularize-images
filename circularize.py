# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 13:33:44 2016

@author: hodges
"""

import argparse
from PIL import Image, ImageOps, ImageDraw

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--diameter', type=int, help='diameter of circular image to be produced')
parser.add_argument('input_files', nargs='+', help='names of input image files')
args = parser.parse_args()
diameter = args.diameter
inputfiles = args.input_files
for infile in inputfiles:
    filename, ext = infile.split('.')
    outfile = filename + '_circular.png'
    size = (diameter, diameter)
    mask = Image.new('L', size, 0)
    draw = ImageDraw.Draw(mask) 
    draw.ellipse((0, 0) + size, fill=255)
    im = Image.open(infile)
    output = ImageOps.fit(im, mask.size, centering=(0.5, 0.5))
    output.putalpha(mask)
    output.save(outfile)
